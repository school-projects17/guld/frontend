import type { Handle, HandleFetch } from "@sveltejs/kit";
import { API_URL } from "./utils/constants";

function redirect(location: string, body?: any) {
    return new Response(body, {
        status: 303,
        headers: { location }
    });
}

const unprotected_routes = [
	"/login",
	"/register"
];

export const handle: Handle = async ({event, resolve}) => {
	const session = event.cookies.get("session");

	if (unprotected_routes.includes(event.url.pathname))
		return resolve(event);

	if (!session)
		return redirect("/login", { message: "Nincs hozzáférésed az oldalhoz!" });
	
	if (session) {
		let {status, msg, result: userdata} = await (await event.fetch(`${API_URL}/user/data`, { method: "GET" })).json();
		if (status === "error")
			return redirect("/login", { message: msg });

		let { categories } = await (await fetch(`${API_URL}/categories`, { method: "GET" })).json();

		event.locals = {
			user: {
				authenticated: true,
				...userdata
			},
			categories
		}
	}
	return resolve(event);
}

export const handleFetch: HandleFetch = async ({ fetch, request, event }) => {
	let session = event.cookies.get("session");
	request.headers.set("content-type", "application/json");
	request.headers.set("cookie", `session=${session}`);
	
	return fetch(request);
}