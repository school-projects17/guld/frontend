import { PUBLIC_API_PORT, PUBLIC_API_HOSTNAME } from "$env/static/public";

export const API_URL = `http://${PUBLIC_API_HOSTNAME}:${PUBLIC_API_PORT}`;

export const COLORS = {
	"success": "#009944",
	"error": "#cf000f",
	"info": "#63c0df",
	"warning": "#f0541e",
}

export const FETCH_OPTIONS: RequestInit = {
	credentials: "include",
	headers: { "content-type": "application/json" },
};