export interface ValidationOptions {
	/**
	 * Whether or not the field is required
	 */
	required: boolean,
	/**
	 * Whether the field should be returned or not
	 */
	return_value: boolean,
	/**
	 * Custom validation regex or function
	 */
	validate?: RegExp | ((data: any) => boolean)
}

export interface ValidationResult {
	field_value: any,
	valid: boolean,
	missing: boolean,
}

function validate_field(field: any, validate: RegExp | ((data: any) => boolean) | undefined) {
	if (typeof(validate) === "object") {
		return validate.test(field);
	} else if (typeof(validate) === "function") {
		return validate(field);
	}

	return true;
}

/**
 * 
 * @param data
 * @param validation_options The object's keys are the fields and the values are the corresponding ValidationOptions
 * @returns [boolean, ValidationResults]
 */
export default async function validate_form(data: FormData, validation_options: {[key: string]: ValidationOptions}): Promise<[boolean, {[key: string]: ValidationResult}]> {
	let missing_or_empty = [...data.keys()].filter(x => !Object.keys(validation_options).includes(x) || data.get(x) === "");

	let fields = Object.fromEntries(
		[...data.entries()]
			.map(([field, value]) =>
				[field, {
					field_value: validation_options[field]?.return_value ? value : "",
					valid: validate_field(value, validation_options[field]?.validate),
					missing: missing_or_empty.includes(field)
				} as ValidationResult]
			)
	);

	return [Object.entries(fields).every(([k, v]) => !v.missing && v.valid), fields];
};