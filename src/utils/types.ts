export interface Spending {
	spending: number,
	category: string
}

export interface Transaction {
	id: string,
	successful: boolean,
	partner_account_number: string,
	partner_account_name: string,
	date: string,
	category: number,
	message: string,
	amount: number
}

export interface Account {
	number: string,
	name: string,
	balance: number,
	category: number | null
}

export interface Bankcard {
	number: string,
	name: string,
	active: boolean,
	take_limit: number,
	purchase_limit: number,
}

export interface Recent {
	number: string,
	name: string
}