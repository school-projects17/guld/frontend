import { invalid, redirect,  } from "@sveltejs/kit";
import { goto } from "$app/navigation";
import type { Actions, PageServerLoad } from "./$types";

import validate_form from "@/utils/form_validation";
// import { send_form_data } from "@/utils/api_request";
import type { ValidationOptions } from "@/utils/form_validation";
import { API_URL } from "@/utils/constants";

export const actions: Actions = {
	default: async ({ request, fetch }) => {
		const data = await request.formData();

		let required_fields = ["id", "firstname", "lastname", "email", "password", "birthdate"];
		let validation_options = Object.fromEntries(required_fields.map(x => [x, { required: true, return_value: x === "password" ? false : true } as ValidationOptions]));
		validation_options["email"].validate = /(.+)@(.+){2,}\.(.+){2,}/;

		let [validation_success, validation_result] = await validate_form(data, validation_options);
		if (!validation_success)
			return invalid(400, { success: validation_success, result: validation_result });

		let request_body = JSON.stringify(Object.fromEntries(data));
		let send_result = await (await fetch(`${API_URL}/user/register`, { 
			method: "POST",
			body: request_body })
		).json();

		if (send_result.status === "error")
			return invalid(400, { success: false, error_msg: send_result.msg, result: validation_result });

		throw redirect(303, "/dashboard");
	}
};