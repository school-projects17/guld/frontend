import type { PageServerLoad, Actions } from "./$types";
import { API_URL } from "@/utils/constants";
import moment from "moment";
import { invalid, redirect } from "@sveltejs/kit";
import type { Spending, Transaction, Account } from "@/utils/types";
import type { ValidationOptions } from "@/utils/form_validation";
import validate_form from "@/utils/form_validation";

export const load: PageServerLoad = async ({ fetch, locals }) => {
	// let { accounts }: {accounts: Array<Account>} = await (await fetch(`${API_URL}/account/get`, { method: "GET" })).json();

	// let request_body = JSON.stringify({
	// 	account_number: accounts[0].number,
	// 	start_date: moment().startOf("month").format("YYYY-MM-DD"),
	// 	end_date: moment().endOf("month").format("YYYY-MM-DD")
	// });
	// let { status, msg, spendings } = await (await fetch(`${API_URL}/account/spending`, {
	// 	method: "POST",
	// 	body: request_body
	// })).json();

	// if (status === "error")
	// 	throw redirect(303, "/login"); // TODO: Error handling

	// let { transactions } = await (await fetch(`${API_URL}/transaction/get`, {
	// 	method: "POST",
	// 	body: request_body
	// })).json();
	
	// return {
	// 	spendings: spendings as Spending[],
	// 	transactions: transactions as Transaction[],
	// 	accounts: accounts as Account[]
	// };
};

export const actions: Actions = {
	add_transaction: async ({ request, fetch }) => {
		let data = await request.formData();

		let required_fields = ["receiver_account", "sender_account", "amount", "message"];
		let validation_options = Object.fromEntries(required_fields.map(x => [x, { required: true, return_value: true } as ValidationOptions]));

		let [validation_success, validation_result] = await validate_form(data, validation_options);
		if (!validation_success)
			return invalid(400, { success: validation_success, result: validation_result });

		let request_body = JSON.stringify(Object.fromEntries(data));
		let send_result = await (await fetch(`${API_URL}/transaction/add`, {
			method: "POST",
			body: request_body
		})).json();

		if (send_result.status === "error")
			return invalid(400, { success: false, error_msg: send_result.msg, result: validation_result });

		return { success: true };
	},
	add_card: async ({request, fetch}) => {
		let data = await request.formData();

		let required_fields = ["account_number", "name", "take_limit", "purchase_limit"];
		let validation_options = Object.fromEntries(required_fields.map(x => [x, { required: true, return_value: true } as ValidationOptions]));

		let [validation_success, validation_result] = await validate_form(data, validation_options);
		if (!validation_success)
			return invalid(400, { success: validation_success, result: validation_result });

		let request_body = JSON.stringify(Object.fromEntries(data));
		let send_result = await (await fetch(`${API_URL}/card/create`, {
			method: "POST",
			body: request_body
		})).json();

		if (send_result.status === "error")
			return invalid(400, { success: false, error_msg: send_result.msg, result: validation_result });

		return { success: true };
	},
	update_card: async ({request, fetch}) => {
		let data = await request.formData();
		
		let required_fields = ["card_number", "name", "take_limit", "purchase_limit"];
		let validation_options = Object.fromEntries(required_fields.map(x => [x, { required: true, return_value: true } as ValidationOptions]));

		let [validation_success, validation_result] = await validate_form(data, validation_options);
		
		if (!validation_success)
			return invalid(400, { success: validation_success, result: validation_result });

		let request_body = JSON.stringify(Object.fromEntries(data));
		let send_result = await (await fetch(`${API_URL}/card/update`, {
			method: "POST",
			body: request_body
		})).json();

		if (send_result.status === "error")
			return invalid(400, { success: false, error_msg: send_result.msg, result: validation_result });

		return { success: true };
	},
	set_category: async ({ request, fetch }) => {
		let data = await request.formData();

		let request_body = JSON.stringify(Object.fromEntries(data));

		await fetch(`${API_URL}/transaction/changecategory`, {
			method: "POST",
			body: request_body
		});

		return { success: true };
	},
	edit_account_name: async ({ request, fetch }) => {
		let data = await request.formData();
		let request_body = JSON.stringify(Object.fromEntries(data));

		await fetch(`${API_URL}/account/update`, {
			method: "POST",
			body: request_body
		});

		return { success: true };
	},
};