import { API_URL } from "@/utils/constants";
import { redirect } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types";

export const load: PageServerLoad = async ({ fetch, cookies }) => {
	await (await fetch(`${API_URL}/user/logout`, { method: "POST" })).json();
	
	cookies.delete("session");
	throw redirect(303, "/login");
};