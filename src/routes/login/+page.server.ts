import type { Actions } from "./$types";

import validate_form from "@/utils/form_validation";
// import { send_form_data } from "@/utils/api_request";
import type { ValidationOptions } from "@/utils/form_validation";

import cookie_parser from "set-cookie-parser";
import type { CookieSerializeOptions } from "cookie";
import { invalid, redirect } from "@sveltejs/kit";
import { API_URL } from "@/utils/constants";

export const actions: Actions = {
	default: async ({ request, cookies, fetch }) => {
		const data = await request.formData();

		let required_fields = ["email", "password"];
		let validation_options = Object.fromEntries(required_fields.map(x => [x, { required: true, return_value: x === "password" ? false : true } as ValidationOptions]));
		validation_options["email"].validate = /(.+)@(.+){2,}\.(.+){2,}/;

		let [validation_success, validation_result] = await validate_form(data, validation_options);
		if (!validation_success)
			return invalid(400, { success: validation_success, result: validation_result });

		let request_body = JSON.stringify(Object.fromEntries(data.entries()));
		let login_result = await fetch(`${API_URL}/user/login`, {
			method: "POST", 
			body: request_body
		});
		
		let login_res_body = await login_result.json();
		
		if (login_res_body.status === "error")
			return invalid(400, { success: false, error_msg: login_res_body.msg, result: validation_result });

		// Forward cookie to the client  
		let cookie = login_result.headers.get("set-cookie");
		
		let {name, value, ...options} = cookie_parser(cookie ?? "")[0];

		cookies.set(name, value, {...options as CookieSerializeOptions, secure: false});
		throw redirect(303, "/dashboard");
	}
};