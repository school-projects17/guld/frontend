import type { PageLoad } from "./$types";

export const load: PageLoad = async ({ url }) => {
	return {
		register_success: url.searchParams.has("register_success")
	}
};
