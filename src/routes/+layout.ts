import type { LayoutLoad } from "./$types";
import { user, categories } from "$lib/stores";

export const load: LayoutLoad =	async ({data}) => {
	user.set(data.user);
	categories.set(data.categories);
}