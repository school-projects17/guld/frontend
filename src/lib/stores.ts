import { writable } from "svelte/store";

export const user = writable<App.Locals["user"]>();
export const categories = writable<App.Locals["categories"]>();