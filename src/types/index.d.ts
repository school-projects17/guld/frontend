declare module "$env/static/public" {
	const PUBLIC_API_PORT: string;
	const PUBLIC_API_HOSTNAME: string;
}